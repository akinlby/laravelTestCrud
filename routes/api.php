<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::apiResource('product', 'ProductController');
Route::get('/upload', [ProductController::class,'upload']);
Route::get('/jobStore', [ProductController::class,'jobStore']);


Route::get('share-post', function() {
    /**
     * Post Oluşturma vb. diğer işlemler
     */
     for ($i=0; $i < 10; $i++) {
         # e-posta göndermek yerine 1 saniye gecikme ekliyoruz.
         sleep(1); 
     }
 
     return 'Gönderiniz yayınlandı';
 });
 
 Route::get('share-post-with-queue', function() {
     /**
     * Post Oluşturma vb. diğer işlemler
     */
     for ($i=0; $i < 10; $i++) { 
         App\Jobs\ProcessProduct::dispatch();
     }
 
     return 'Gönderiniz yayınlandı.';
 });


